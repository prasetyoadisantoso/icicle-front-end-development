<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="blog" />
    <meta name="author" content="prasetyo" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome | Portfolio</title>
    <link rel="icon" type="image/x-icon" href="{{asset('assets/resume/img/favicon.ico')}}" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet"
        type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{asset('assets/resume/css/styles.css')}}" rel="stylesheet" />
    {{-- Owlcarousel --}}
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/owlcarousel/css/owl.theme.default.min.css')}}">
</head>

<body id="page-top">


    {{-- php section --}}
    @php
    use Modules\Portfolio\Classes\PortfolioClass;
    $latest_portfolio = PortfolioClass::getPortfolioPosts();
    @endphp


    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-olive fixed-top" id="sideNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">
            <span class="d-block d-lg-none"><img class="img-profile rounded-circle mx-auto my-auto"
                    src="{{asset('assets/blog/img/pras.jpg')}}" alt="" style="width: 50px; height:auto;" />&nbsp; My
                Personal Blog</span>
            <span class="d-none d-lg-block"><img class="img-fluid img-profile rounded-circle mx-auto mb-2"
                    src="{{asset('assets/blog/img/pras.jpg')}}" alt="" /></span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">Home</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger " href="{{url('/')}}">Blog</a></li>
                @foreach ($latest_portfolio as $item)
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#{{$item->title}}">{{$item->title}}</a>
                </li>
                @endforeach
            </ul>
        </div>
    </nav>

    <!-- Page Content-->
    <div class="container">
        <div class="container-fluid p-0">
            <!-- About-->
            <section class="resume-section" id="about">
                <div class="resume-section-content">
                    <h2 class="mb-0">
                        My
                        <span class="text-olive">Portfolio</span>
                    </h2>
                    <div class="subheading mb-5">
                        You can email to me at :
                        <a href="mailto:name@email.com">prasetyoadisantoso@gmail.com</a>
                    </div>
                    <p class="lead">I am experienced in Web Programming for 1 Year</p>
                    <p class="lead mb-5">You can see my portfolio below<i class="far fa-smile-wink ml-3"></i></p>
                    <div class="subheading mb-3">Programming Languages & Tools</div>
                    <ul class="list-inline dev-icons">
                        <li class="list-inline-item"><i class="fab fa-html5"></i></li>
                        <li class="list-inline-item"><i class="fab fa-css3-alt"></i></li>
                        <li class="list-inline-item"><i class="fab fa-js-square"></i></li>
                        <li class="list-inline-item"><i class="fab fa-node-js"></i></li>
                        <li class="list-inline-item"><i class="fab fa-sass"></i></li>
                        <li class="list-inline-item"><i class="fab fa-less"></i></li>
                        <li class="list-inline-item"><i class="fab fa-npm"></i></li>
                        <li class="list-inline-item"><i class="fab fa-php"></i></li>
                        <li class="list-inline-item"><i class="fab fa-wordpress"></i></li>
                        <li class="list-inline-item"><i class="fab fa-laravel"></i></li>
                    </ul>
                </div>
            </section>
            <hr class="m-0" />

            @foreach ($latest_portfolio as $item)
            <section class="resume-section" id="{{$item->title}}">
                <div class="resume-section-content">

                    <div class="card border-0">
                        <div class="owl-carousel owl-theme">
                            <img src="{{asset('uploads/'. $item->image_1)}}" class="card-img-top item" alt="">
                            <img src="{{asset('uploads/'. $item->image_2)}}" class="card-img-top item" alt="">
                            <img src="{{asset('uploads/'. $item->image_3)}}" class="card-img-top item" alt="">
                            <img src="{{asset('uploads/'. $item->image_4)}}" class="card-img-top item" alt="">
                            <img src="{{asset('uploads/'. $item->image_5)}}" class="card-img-top item" alt="">
                        </div>

                        <div class="card-body">
                            <h2 class="card-title text-center text-olive">{{$item->title}}</h2>
                            <p class="card-text">{{$item->content}}</p>
                        </div>
                    </div>

                </div>
            </section>
            <hr class="m-0" />
            @endforeach




        </div>
    </div>

    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Core theme JS-->
    <script src="{{asset('assets/resume/js/scripts.js')}}"></script>
    {{-- OwlCarousel --}}
    <script src="{{asset('assets/owlcarousel/js/owl.carousel.min.js')}}"></script>

    <script>
        $('.owl-carousel').owlCarousel({
            loop:true,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>

</body>

</html>
