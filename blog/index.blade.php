<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="blog">
    <meta name="author" content="prasetyo">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Welcome | Home</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/blog/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{asset('assets/blog/vendor/fontawesome-free/css/all.min.cs')}}s" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet'
        type='text/css'>
    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
        rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{{asset('assets/blog/css/clean-blog.min.css')}}" rel="stylesheet">

</head>

<style>
    /* Pagination */
    .page-item.active .page-link {
        background-color: #3d9970ff !important;
        border-color: #3d9970ff !important;
    }



    @media only screen and (max-width: 600px) {
        div img {
            width: 100% !important;
            height: auto !important;
        }
    }

    img:hover {
        -ms-transform: scale(1.5);
        /* IE 9 */
        -webkit-transform: scale(1.5);
        /* Safari 3-8 */
        transform: scale(1.5);
    }


</style>

<body>

    {{-- Get class from Modules CMS --}}
    @php
    use \Modules\CMS\Classes\PostClass;
    use \Modules\CMS\Classes\CategoriesClass;
    $post = PostClass::getPosts();
    $category = CategoriesClass::getCategories();
    @endphp

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">Home</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                        href="{{url('portfolios')}}">Portfolio</a></li>

                    {{-- If user roles empty show it --}}
                    @if (empty($user))
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="{{route('login')}}">Login</a></li>

                    {{-- If user roles Administrator show it --}}
                    @elseif ($user->roles()->pluck('name')[0] == 'Administrator')

                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="{{route('administrator')}}">Dashboard</a></li>


                    {{-- If user roles not administrator show it --}}
                    @elseif ($user->roles()->pluck('name') != 'Administrator')
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="{{url('administrator/logout')}}">Logout</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('assets/blog/img/about-bg.jpg')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <img class="masthead-avatar" src="{{asset('assets/blog/img/pras.jpg')}}" alt=""
                            style="max-width: 150px;" />
                        <h1>Prasetyo Adi Santoso Blog</h1>
                        <span class="subheading">Powered by ICICLE Laravel Admin Panel</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">


            <div class="order-sm-2 col-sm-12 col-md-3">
                <div class="container">
                    <h3>Categories</h3>
                </div>
                <ul class="list-group mb-5">
                    @foreach ($category as $item)
                    @if ($item->posts->count() == 0)



                    @else
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="{{route('post.cat', $item->category)}}">{{$item->category}}</a>
                        <span class="badge badge-success badge-pill">{{$item->posts->count()}}</span>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>



            <div class="order-sm-1 col-sm-12 col-md-9">

                {{-- Post Item --}}
                @foreach ($post as $item)
                <div class="post-preview">
                    <div class="post-title">
                        <a href="{{route('posting', $item->title)}}">
                            <h1>{!!$item->title!!}</h1>
                        </a>
                        <small style="font-weight: bold">{!!$item->created_at->format('M d, Y');!!}</small>
                        {{-- category of posts --}}
                        <small> | Category:</small>
                        @foreach ($item->categories as $categori)
                        <small>{{$categori->category}}</small>
                        @endforeach
                    </div>
                    <div class="card-body">
                        {!!$item->content!!}
                    </div>
                </div>
                @endforeach
                {{-- End Post Item --}}

                <!-- Pagination -->
                <div class="clearfix">
                    {{$post->links()}}
                </div>

            </div>





        </div>
    </div>

    <hr>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <ul class="list-inline text-center">
                        <li class="list-inline-item">
                            <a href="https://gitlab.com/prasetyo.element">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-gitlab fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://web.facebook.com/pras.element">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://github.com/purashichiyo">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Copyright &copy; Prasetyo Adi Santoso | Version beta-7.8.15 | 2020
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('assets/blog/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/blog/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset('assets/blog/js/clean-blog.min.js')}}"></script>

</body>

</html>
