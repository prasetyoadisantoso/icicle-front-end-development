<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Icicle | Posts</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/blog/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{asset('assets/blog/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet'
        type='text/css'>
    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
        rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{{asset('assets/blog/css/clean-blog.min.css')}}" rel="stylesheet">

</head>

<style>
    @media only screen and (min-width: 768px) {

        header.masthead .page-heading,
        header.masthead .post-heading,
        header.masthead .site-heading {
            padding: 100px 0;
        }
    }

    @media only screen and (max-width: 600px) {
          div img {
            width: 100% !important;
            height: auto !important;
        }
    }

    img:hover {
        -ms-transform: scale(1.5);
        /* IE 9 */
        /* -webkit-transform: scale(2.0); */
        /* Safari 3-8 */
        transform: scale(2.0);
    }

</style>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">Home</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">

                    {{-- If user roles empty show it --}}
                    @if (empty($user))
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="{{route('login')}}">Login</a></li>

                    {{-- If user roles Administrator show it --}}
                    @elseif ($user->roles()->pluck('name')[0] == 'Administrator')

                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="{{route('administrator')}}">Dashboard</a></li>


                    {{-- If user roles not administrator show it --}}
                    @elseif ($user->roles()->pluck('name') != 'Administrator')
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="{{url('administrator/logout')}}">Logout</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Header -->
    <header class="masthead" class="masthead" style="background-image: url('assets/blog/img/web-design.jpg')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="site-heading">
                        <h2>Posts by {{$category}}</h2>
                        {{-- <span class="subheading"></span> --}}
                    </div>
                </div>
            </div>
        </div>
    </header>


    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-12">

                @foreach ($posts as $item)
                <div class="post-preview">
                    <a href="{{route('posting', $item->title)}}">
                        <h1>{!!$item->title!!}</h1>
                    </a>
                    <small style="font-weight: bold">{!!$item->created_at->format('M d, Y');!!}</small>

                    <div class="card-body">
                        {!!$item->content!!}
                    </div>

                </div>
                @endforeach

            </div>
        </div>
    </div>

    {{-- Post Item --}}
    {{-- @foreach ($post as $item)
                <div class="post-preview">
                    <div class="post-title">

                        @foreach ($item->categories as $categori)
                        <small>{{$categori->category}}</small>
    @endforeach
    </div>
    <div class="card-body">
        {!!$item->content!!}
    </div>
    </div>
    @endforeach --}}
    {{-- End Post Item --}}



    <hr>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <ul class="list-inline text-center">
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <p class="copyright text-muted">Copyright &copy; Your Website 2020</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('assets/blog/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/blog/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset('assets/blog/js/clean-blog.min.js')}}"></script>

</body>

</html>
